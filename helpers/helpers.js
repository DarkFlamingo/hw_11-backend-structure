const { transformData } = require('./transform-data/transform-data.helper');
const {
  definePrediction,
} = require('./define-prediction/define-prediction.helper');
const {
  switchPrediction,
} = require('./switch-prediction/switch-prediction.helper');

module.exports = { transformData, definePrediction, switchPrediction };
