const transformData = (fieldArray, resultArray) => {
  fieldArray.forEach((whatakey) => {
    const index = whatakey.indexOf('_');
    let newKey = whatakey.replace('_', '');
    newKey = newKey.split('');
    newKey[index] = newKey[index].toUpperCase();
    newKey = newKey.join('');
    resultArray[newKey] = resultArray[whatakey];
    delete resultArray[whatakey];
  });
};

module.exports = { transformData }
