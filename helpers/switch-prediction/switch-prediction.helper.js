const switchPrediction = (value, odds) => {
  if (value === 'w1') {
    return odds.home_win;
  } else if (value === 'w2') {
    return odds.away_win;
  } else if (value === 'x') {
    return odds.draw;
  }
};

module.exports = { switchPrediction };
