const definePrediction = ([w1, w2]) => {
  if (+w1 > +w2) {
    return 'w1';
  } else if (+w2 > +w1) {
    return 'w2';
  } else {
    return 'x';
  }
};

module.exports = { definePrediction };
