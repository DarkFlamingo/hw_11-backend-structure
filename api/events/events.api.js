const { EventApiPath } = require('../../common/enums/api/event-api-path.enum');
const { eventValidator } = require('../../middlewares/validators/validators');
const { HttpError } = require('../../exceptions/exceptions');
const { jwtMiddlewareByRole } = require('../../middlewares/jwt/jwt.middleware');
const { definePrediction } = require('../../helpers/helpers');

const initEvents = (Router, services) => {
  const {
    OddsService: oddsService,
    EventService: eventService,
    BetService: betService,
    UserService: userService,
  } = services;
  const router = Router();

  router.post(
    EventApiPath.ROOT,
    eventValidator.PostEventValidator,
    jwtMiddlewareByRole,
    (req, res, next) => {
      req.body.odds.home_win = req.body.odds.homeWin;
      delete req.body.odds.homeWin;
      req.body.odds.away_win = req.body.odds.awayWin;
      delete req.body.odds.awayWin;
      oddsService
        .addOdds(req.body)
        .then((result) => res.send(result))
        .catch(next);
    }
  );

  router.put(
    EventApiPath.$ID,
    eventValidator.PutEventValidator,
    jwtMiddlewareByRole,
    (req, res, next) => {
      betService
        .getBets({ event_id: req.params.id, win: null })
        .then((bets) => {
          const result = definePrediction(req.body.score.split(':'));
          eventService
            .updateEvent(
              { id: req.params.id, score: req.body.score },
              bets,
              result
            )
            .then((result) => res.send(result))
            .catch(next);
        });
    }
  );
  return router;
};

module.exports = { initEvents };
