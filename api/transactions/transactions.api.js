const {
  TransactionApiPath,
} = require('../../common/enums/api/transaction-api-path.enum');
const {
  transactionValidator,
} = require('../../middlewares/validators/validators');
const { HttpError } = require('../../exceptions/exceptions');
const { jwtMiddlewareByRole } = require('../../middlewares/jwt/jwt.middleware');
const { transformData } = require('../../helpers/helpers');

const initTransaction = (Router, services) => {
  const { TransactionService: transactionService, UserService: userService } =
    services;
  const router = Router();

  router.post(
    TransactionApiPath.ROOT,
    transactionValidator.PostTransactionValidator,
    jwtMiddlewareByRole,
    (req, res, next) => {
      console.log(req.body.userId);
      userService
        .getUserById(req.body.userId)
        .then(([user]) => {
          if (!user) {
            throw new HttpError({
              status: 400,
              message: 'User does not exist',
            });
          } else {
            req.body.card_number = req.body.cardNumber;
            delete req.body.cardNumber;
            req.body.user_id = req.body.userId;
            delete req.body.userId;
            transactionService
              .addTransaction(req.body, user)
              .then((result) => res.send(result))
              .catch(next);
          }
        })
        .catch(next);
    }
  );

  return router;
};

module.exports = { initTransaction };
