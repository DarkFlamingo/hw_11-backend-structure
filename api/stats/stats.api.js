const { StatsApiPath } = require('../../common/enums/api/stats-api-path.enum');
const { HttpError } = require('../../exceptions/exceptions');
const { jwtMiddlewareByRole } = require('../../middlewares/jwt/jwt.middleware');

const initUser = (Router, services) => {
  const {} = services;
  const router = Router();

  router.get(StatsApiPath.ROOT, jwtMiddlewareByRole, (req, res, next) => {});
  return router;
};

module.exports = { initUser };
