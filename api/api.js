const { ApiPath } = require('../common/enums/api/api-path.enum');
const { initUser } = require('./users/users.api');
const { initTransaction } = require('./transactions/transactions.api');
const { initEvents } = require('./events/events.api');
const { initBet } = require('./bets/bets.api');
const {
  UserService,
  BetService,
  EventService,
  TransactionService,
  AuthService,
  OddsService,
} = require('../services/services');

const initApi = (Router) => {
  const apiRouter = Router();

  apiRouter.use(ApiPath.USERS, initUser(Router, { UserService, AuthService }));
  apiRouter.use(
    ApiPath.TRANSACTIONS,
    initTransaction(Router, { TransactionService, UserService })
  );
  apiRouter.use(
    ApiPath.EVENTS,
    initEvents(Router, { OddsService, EventService, BetService, UserService })
  );
  apiRouter.use(
    ApiPath.BETS,
    initBet(Router, { BetService, UserService, OddsService, EventService })
  );

  return apiRouter;
};

module.exports = { initApi };
