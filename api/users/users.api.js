const { UserApiPath } = require('../../common/enums/api/user-api-path.enum');
const { userValidator } = require('../../middlewares/validators/validators');
const { HttpError } = require('../../exceptions/exceptions');
const { jwtMiddleware } = require('../../middlewares/jwt/jwt.middleware');

const initUser = (Router, services) => {
  const { UserService: userService, AuthService: authService } = services;
  const router = Router();

  router
    .get(UserApiPath.$ID, userValidator.GetUserValidator, (req, res, next) => {
      userService
        .getUserById(req.params.id)
        .then(([result]) => {
          if (!result) {
            throw new HttpError({ status: 404, message: 'User not found' });
          } else {
            res.status(200).send({ ...result });
          }
        })
        .catch(next);
    })
    .post(
      UserApiPath.ROOT,
      userValidator.PostUserValidator,
      (req, res, next) => {
        console.log(req.body)
        userService
          .create(req.body)
          .then((result) => {
            const accessToken = authService.login(result);
            res.send({
              ...result,
              accessToken,
            });
          })
          .catch((err) => {
            if (err.code == '23505') {
              throw new HttpError({ status: 400, message: err.detail });
            }
            next(err);
          })
          .catch(next);;
      }
    )
    .put(
      UserApiPath.$ID,
      jwtMiddleware,
      userValidator.PutUserValidator,
      (req, res, next) => {
        userService
          .update({ id: req.params.id, ...req.body })
          .then((result) => {
            res.send({ ...result });
          })
          .catch((err) => {
            if (err.code === '23505') {
              throw new HttpError({ status: 400, message: err.detail });
            }
          })
          .catch(next);
      }
    );

  return router;
};

module.exports = { initUser };
