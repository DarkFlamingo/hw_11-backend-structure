const { BetApiPath } = require('../../common/enums/api/bet-api-path.enum');
const { betValidator } = require('../../middlewares/validators/validators');
const { HttpError } = require('../../exceptions/exceptions');
const { jwtMiddlewareDouble } = require('../../middlewares/jwt/jwt.middleware');
const { switchPrediction } = require('../../helpers/helpers');

const initBet = (Router, services) => {
  const {
    UserService: userService,
    BetService: betService,
    EventService: eventService,
    OddsService: oddsService,
  } = services;
  const router = Router();

  router.post(
    BetApiPath.ROOT,
    betValidator.PostBetValidator,
    jwtMiddlewareDouble,
    (req, res, next) => {
      const userId = req.jwtUserId;
      req.body.event_id = req.body.eventId;
      req.body.bet_amount = req.body.betAmount;
      delete req.body.eventId;
      delete req.body.betAmount;
      req.body.user_id = userId;
      userService
        .getUserById(userId)
        .then(([user]) => {
          if (!user) {
            throw new HttpError({
              status: 400,
              message: 'User does not exist',
            });
          }
          if (+user.balance < +req.body.bet_amount) {
            throw new HttpError({ status: 400, message: 'Not enough balance' });
          }
          eventService
            .getEventById(req.body.event_id)
            .then(([event]) => {
              if (!event) {
                throw new HttpError({
                  status: 404,
                  message: 'Event not found',
                });
              }
              oddsService
                .getOddById(event.odds_id)
                .then(([odds]) => {
                  if (!odds) {
                    throw new HttpError({
                      status: 404,
                      message: 'Odds not found',
                    });
                  }
                  const multiplier = switchPrediction(req.body.prediction, odds);
                  betService
                    .addBets({
                      ...req.body,
                      multiplier,
                      event_id: event.id,
                    }, user, userId)
                    .then((result) => res.send(result))
                    .catch(next);
                })
                .catch(next);
            })
            .catch(next);
        })
        .catch(next);
    }
  );

  return router;
};

module.exports = { initBet };
