const initBet = (orm) => {
  const Bet = orm
    .object({
      id: orm.string().uuid(),
      eventId: orm.string().uuid().required(),
      userId: orm.string().uuid(),
      betAmount: orm.number().min(1),
      prediction: orm.string().valid('w1', 'w2', 'x').required(),
      multiplier: orm.number(),
      win: orm.boolean(),
      createdAt: orm.date(),
      updatedAt: orm.date(),
    })
    .required();

  return Bet;
};

module.exports = { initBet };
