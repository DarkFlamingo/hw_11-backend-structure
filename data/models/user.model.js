const initUser = (orm) => {
  const User = orm
    .object({
      id: orm.string().uuid(),
      type: orm.string().required(),
      email: orm.string().email().required(),
      phone: orm
        .string()
        .pattern(/^\+?3?8?(0\d{9})$/)
        .required(),
      name: orm.string().required(),
      city: orm.string(),
      balance: orm.string(),
      created_at: orm.date(),
      updated_at: orm.date(),
    })
    .required();

  return User;
};

module.exports = { initUser };
