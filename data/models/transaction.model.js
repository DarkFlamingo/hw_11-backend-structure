const initTransaction = (orm) => {
  const Transaction = orm
    .object({
      id: orm.string().uuid(),
      userId: orm.string().uuid().required(),
      cardNumber: orm.string().required(),
      amount: orm.number().min(0).required(),
      createdAt: orm.date(),
      updatedAt: orm.date(),
    })
    .required();

  return Transaction;
};

module.exports = { initTransaction };
