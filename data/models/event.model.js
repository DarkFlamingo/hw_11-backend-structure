const initEvent = (orm) => {
  const Event = orm
    .object({
      id: orm.string().uuid(),
      type: orm.string().required(),
      homeTeam: orm.string().required(),
      awayTeam: orm.string().required(),
      startAt: orm.date().required(),
      odds: orm
        .object({
          homeWin: orm.number().min(1.01).required(),
          awayWin: orm.number().min(1.01).required(),
          draw: orm.number().min(1.01).required(),
        })
        .required(),
      score: orm.string(),
      createdAt: orm.date(),
      updatedAt: orm.date(),
    })
    .required();

  return Event;
};

module.exports = { initEvent };
