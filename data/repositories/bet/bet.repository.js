const Abstract = require('../abstract/abstract.repository');

class BetRepository extends Abstract {
  constructor(db) {
    super(db, 'bet');
  }
}

module.exports = BetRepository;
