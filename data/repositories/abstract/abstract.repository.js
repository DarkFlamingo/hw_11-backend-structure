class Abstract {
  constructor(db, table) {
    this.db = db;
    this.table = table;
  }

  getAll(where = {}) {
    return this.db(this.table).select().where(where);
  }

  getById(id) {
    return this.db(this.table).where({ id }).returning('*');
  }

  create(data) {
    return this.db(this.table)
      .insert(data)
      .returning('*')
      .then(([result]) => {
        result.createdAt = result.created_at;
        delete result.created_at;
        result.updatedAt = result.updated_at;
        delete result.updated_at;
        return { ...result };
      })
      .catch((err) => {
        throw err;
      });
  }

  deleteById(id) {
    return this.db(this.table)
      .where({ id })
      .del()
      .then(() => true)
      .catch(() => false);
  }

  updateById(data) {
    return this.db(this.table)
      .where({ id: data.id })
      .update(data)
      .returning('*')
      .then(([result]) => {
        return { ...result };
      })
      .catch((err) => {
        throw err;
      });
  }
}

module.exports = Abstract;
