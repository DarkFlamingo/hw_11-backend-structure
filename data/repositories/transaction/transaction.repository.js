const Abstract = require('../abstract/abstract.repository');

class TransactionRepository extends Abstract {
  constructor(db) {
    super(db, 'transaction');
  }
}

module.exports = TransactionRepository;
