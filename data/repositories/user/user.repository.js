const Abstract = require('../abstract/abstract.repository');

class UserRepository extends Abstract {
  constructor(db) {
    super(db, 'user');
  }
}

module.exports = UserRepository;
