const { db } = require('../db/connection');
const BetRepository = require('./bet/bet.repository');
const UserRepository = require('./user/user.repository');
const EventRepository = require('./event/event.repository');
const TransactionRepository = require('./transaction/transaction.repository');
const OddsRepository = require('./odds/odds.repository');

const Bet = new BetRepository(db);
const User = new UserRepository(db);
const Event = new EventRepository(db);
const Transaction = new TransactionRepository(db);
const Odds = new OddsRepository(db);

module.exports = {
  Bet,
  User,
  Event,
  Transaction,
  Odds,
};
