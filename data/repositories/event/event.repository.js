const Abstract = require('../abstract/abstract.repository');

class EventRepository extends Abstract {
  constructor(db) {
    super(db, 'event');
  }
}

module.exports = EventRepository;
