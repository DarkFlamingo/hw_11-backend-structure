const Abstract = require('../abstract/abstract.repository');

class OddsRepository extends Abstract {
  constructor(db) {
    super(db, 'odds');
  }
}

module.exports = OddsRepository;
