const { transformData } = require('../../helpers/helpers');

class BetService {
  constructor({ betRepository, userRepository }) {
    this._betRepository = betRepository;
    this._userRepository = userRepository;
  }

  getBets(filter) {
    return this._betRepository.getAll(filter);
  }

  getBetById(id) {
    return this._betRepository.getById(id);
  }

  create(id) {
    return this._betRepository.create(id);
  }

  delete(id) {
    return this._betRepository.deleteById(id);
  }

  update(data) {
    return this._betRepository.updateById(data);
  }

  async addBets(data, user, userId) {
    const bet = await this._betRepository.create(data);
    let currentBalance = user.balance - data.bet_amount;
    await this._userRepository.updateById({
      id: userId,
      balance: currentBalance,
    });
    transformData(
      [
        'bet_amount',
        'event_id',
        'away_team',
        'home_team',
        'odds_id',
        'start_at',
        'user_id',
      ],
      bet
    );
    return {
      ...bet,
      currentBalance: currentBalance,
    };
  }
}

module.exports = BetService;
