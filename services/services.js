const EventServiceClass = require('./event/event.service');
const BetServiceClass = require('./bet/bet.service');
const TransactionServiceClass = require('./transaction/transaction.service');
const UserServiceClass = require('./user/user.service');
const AuthServiceClass = require('./auth/auth.service');
const OddsServiceClass = require('./odds/odds.service');
const {
  Bet,
  User,
  Event,
  Transaction,
  Odds,
} = require('../data/repositories/repositories');

const UserService = new UserServiceClass({ userRepository: User });
const BetService = new BetServiceClass({
  betRepository: Bet,
  userRepository: User,
});
const EventService = new EventServiceClass({
  eventRepository: Event,
  userRepository: User,
  betRepository: Bet,
});
const TransactionService = new TransactionServiceClass({
  transactionRepository: Transaction,
  userRepository: User,
});
const AuthService = new AuthServiceClass();
const OddsService = new OddsServiceClass({
  oddsRepository: Odds,
  eventRepository: Event,
});

module.exports = {
  UserService,
  BetService,
  EventService,
  TransactionService,
  AuthService,
  OddsService,
};
