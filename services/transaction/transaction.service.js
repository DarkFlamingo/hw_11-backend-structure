const { transformData } = require('../../helpers/helpers');

class TransactionService {
  constructor({ transactionRepository, userRepository }) {
    this._transactionRepository = transactionRepository;
    this._userRepository = userRepository;
  }

  getTransactions(filter) {
    return this._transactionRepository.getAll(filter);
  }

  getTransactionById(id) {
    return this._transactionRepository.getById(id);
  }

  create(data) {
    return this._transactionRepository.create(data);
  }

  delete(id) {
    return this._transactionRepository.deleteById(id);
  }

  update(data) {
    return this._transactionRepository.updateById(data);
  }

  async addTransaction(data, user) {
    const result = await this._transactionRepository.create(data);
    const currentBalance = data.amount + user.balance;
    const updatedUser = await this._userRepository.updateById({
      id: data.user_id,
      balance: currentBalance,
    });
    transformData(['user_id', 'card_number'], result);
    return {
      ...result,
      currentBalance,
    };
  }
}

module.exports = TransactionService;
