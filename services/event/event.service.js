const { transformData } = require('../../helpers/helpers');

class EventService {
  constructor({ eventRepository, betRepository, userRepository }) {
    this._eventRepository = eventRepository;
    this._betRepository = betRepository;
    this._userRepository = userRepository;
  }

  getEvents(filter) {
    return this._eventRepository.getAll(filter);
  }

  getEventById(id) {
    return this._eventRepository.getById(id);
  }

  create(id) {
    return this._eventRepository.create(id);
  }

  delete(id) {
    return this._eventRepository.deleteById(id);
  }

  update(data) {
    return this._eventRepository.updateById(data);
  }

  async updateEvent(data, bets, result) {
    const event = await this._eventRepository.updateById(data);
    await Promise.all(
      bets.map((bet) => {
        if (bet.prediction == result) {
          this._betRepository.updateById({ id: bet.id, win: true });
          this._userRepository.getById(bet.user_id).then(([user]) => {
            return this._userRepository.updateById({
              id: bet.user_id,
              balance: user.balance + bet.bet_amount * bet.multiplier,
            });
          });
        } else if (bet.prediction != result) {
          return this._betRepository.updateById({ id: bet.id, win: false });
        }
      })
    );
    const promise = new Promise((resolve, reject) =>
      setTimeout(() => {
        transformData(
          [
            'bet_amount',
            'event_id',
            'away_team',
            'home_team',
            'odds_id',
            'start_at',
            'updated_at',
            'created_at',
          ],
          event
        );
        resolve(event);
      }, 1000)
    );
    return promise;
  }
}

module.exports = EventService;
