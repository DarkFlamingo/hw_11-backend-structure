const jwt = require('jsonwebtoken');
const { ENV } = require('../../common/enums/app/env.enum');

class AuthService {
  login(data) {
    return jwt.sign({ id: data.id, type: data.type }, ENV.JWT.SECRET);
  }

  verify(token) {
    return jwt.verify(token, ENV.JWT.SECRET);
  }
}

module.exports = AuthService;
