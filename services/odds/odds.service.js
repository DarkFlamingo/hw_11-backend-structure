const { transformData } = require('../../helpers/helpers');

class OddsService {
  constructor({ oddsRepository, eventRepository }) {
    this._oddsRepository = oddsRepository;
    this._eventRepository = eventRepository;
  }

  getOdds(filter) {
    return this._oddsRepository.getAll(filter);
  }

  getOddById(id) {
    return this._oddsRepository.getById(id);
  }

  create(id) {
    return this._oddsRepository.create(id);
  }

  delete(id) {
    return this._oddsRepository.deleteById(id);
  }

  update(data) {
    return this._oddsRepository.updateById(data);
  }

  async addOdds(data) {
    const odds = await this._oddsRepository.create(data.odds);
    delete data.odds;
    data.away_team = data.awayTeam;
    data.home_team = data.homeTeam;
    data.start_at = data.startAt;
    delete data.awayTeam;
    delete data.homeTeam;
    delete data.startAt;
    const event = await this._eventRepository.create({
      ...data,
      odds_id: odds.id,
    });
    transformData(
      [
        'bet_amount',
        'event_id',
        'away_team',
        'home_team',
        'odds_id',
        'start_at',
      ],
      event
    );
    transformData(['home_win', 'away_win'], odds);
    return {
      ...event,
      odds,
    };
  }
}

module.exports = OddsService;
