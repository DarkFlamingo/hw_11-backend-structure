const { HttpError } = require('../../exceptions/exceptions');
const { AuthService } = require('../../services/services');

const jwtMiddleware = (req, res, next) => {
  let token = req.headers['authorization'];
  let tokenPayload;
  if (!token) {
    throw new HttpError({ status: 401, message: 'Not Authorized' });
  }
  token = token.replace('Bearer ', '');
  try {
    tokenPayload = AuthService.verify(token);
  } catch (err) {
    throw new HttpError({ status: 401, message: 'Not Authorized' });
  }
  if (req.params.id !== tokenPayload.id) {
    throw new HttpError({ status: 401, message: 'UserId mismatch' });
  }
  next();
};

const jwtMiddlewareByRole = (req, res, next) => {
  let token = req.headers['authorization'];
  if (!token) {
    throw new HttpError({ status: 401, message: 'Not Authorized' });
  }
  token = token.replace('Bearer ', '');
  tokenPayload = AuthService.verify(token);
  if (tokenPayload.type != 'admin') {
    throw new HttpError({ status: 401, message: 'Not Authorized' });
  }
  next();
};

const jwtMiddlewareDouble = (req, res, next) => {
  let token = req.headers['authorization'];
  if (!token) {
    throw new HttpError({ status: 401, message: 'Not Authorized' });
  }
  token = token.replace('Bearer ', '');
  tokenPayload = AuthService.verify(token);
  if (!tokenPayload) {
    throw new HttpError({ status: 401, message: 'Not Authorized' });
  }
  req.jwtUserId = tokenPayload.id;
  next();
};

module.exports = { jwtMiddleware, jwtMiddlewareByRole, jwtMiddlewareDouble };
