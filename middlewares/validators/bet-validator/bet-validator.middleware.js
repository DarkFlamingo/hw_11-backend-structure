const joi = require('joi');
const { HttpError } = require('../../../exceptions/exceptions');

const DefaultValidationResult = (validationSchema, data, next) => {
  const isValidResult = validationSchema.validate(data);
  if (isValidResult.error) {
    throw new HttpError({
      status: 400,
      message: isValidResult.error.details[0].message,
    });
  } else {
    next();
  }
};

const PostBetValidator = (req, res, next) => {
  const validationSchema = joi
    .object({
      id: joi.string().uuid(),
      eventId: joi.string().uuid().required(),
      betAmount: joi.number().min(1).required(),
      prediction: joi.string().valid('w1', 'w2', 'x').required(),
    })
    .required();
  DefaultValidationResult(validationSchema, req.body, next);
};

module.exports = {
  PostBetValidator,
};
