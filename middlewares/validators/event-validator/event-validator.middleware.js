const joi = require('joi');
const { HttpError } = require('../../../exceptions/exceptions');

const DefaultValidationResult = (validationSchema, data, next) => {
  const isValidResult = validationSchema.validate(data);
  if (isValidResult.error) {
    throw new HttpError({
      status: 400,
      message: isValidResult.error.details[0].message,
    });
  } else {
    next();
  }
};

const PostEventValidator = (req, res, next) => {
  const validationSchema = joi
    .object({
      id: joi.string().uuid(),
      type: joi.string().required(),
      homeTeam: joi.string().required(),
      awayTeam: joi.string().required(),
      startAt: joi.date().required(),
      odds: joi
        .object({
          homeWin: joi.number().min(1.01).required(),
          awayWin: joi.number().min(1.01).required(),
          draw: joi.number().min(1.01).required(),
        })
        .required(),
    })
    .required();
  DefaultValidationResult(validationSchema, req.body, next);
};

const PutEventValidator = (req, res, next) => {
  const validationSchema = joi
    .object({
      score: joi.string().required(),
    })
    .required();
  DefaultValidationResult(validationSchema, req.body, next);
};

module.exports = {
  PostEventValidator,
  PutEventValidator,
};
