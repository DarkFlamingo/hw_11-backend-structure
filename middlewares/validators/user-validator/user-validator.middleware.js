const joi = require('joi');
const { HttpError } = require('../../../exceptions/exceptions');

const DefaultValidationResult = (
  validationSchema,
  data,
  next,
  balance = false
) => {
  const isValidResult = validationSchema.validate(data);
  if (isValidResult.error) {
    throw new HttpError({
      status: 400,
      message: isValidResult.error.details[0].message,
    });
  } else {
    if (balance) {
      data.balance = 0;
    }
    next();
  }
};

const GetUserValidator = (req, res, next) => {
  const validationSchema = joi
    .object({
      id: joi.string().uuid(),
    })
    .required();
  DefaultValidationResult(validationSchema, req.params, next);
};

const PostUserValidator = (req, res, next) => {
  const validationSchema = joi
    .object({
      id: joi.string().uuid(),
      type: joi.string().required(),
      email: joi.string().email().required(),
      phone: joi
        .string()
        .pattern(/^\+?3?8?(0\d{9})$/)
        .required(),
      name: joi.string().required(),
      city: joi.string(),
    })
    .required();
  DefaultValidationResult(validationSchema, req.body, next, true);
};

const PutUserValidator = (req, res, next) => {
  const validationSchema = joi
    .object({
      email: joi.string().email(),
      phone: joi.string().pattern(/^\+?3?8?(0\d{9})$/),
      name: joi.string(),
      city: joi.string(),
    })
    .required();
  DefaultValidationResult(validationSchema, req.body, next);
};

module.exports = {
  GetUserValidator,
  PostUserValidator,
  PutUserValidator,
};
