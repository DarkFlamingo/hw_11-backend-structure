const {
  GetUserValidator,
  PostUserValidator,
  PutUserValidator,
} = require('./user-validator/user-validator.middleware');
const {
  PostTransactionValidator,
} = require('./transaction-validator/transaction-validator.middleware');
const {
  PostEventValidator,
  PutEventValidator,
} = require('./event-validator/event-validator.middleware');
const {
  PostBetValidator,
} = require('./bet-validator/bet-validator.middleware');

module.exports = {
  userValidator: {
    GetUserValidator,
    PostUserValidator,
    PutUserValidator,
  },
  transactionValidator: {
    PostTransactionValidator,
  },
  eventValidator: {
    PostEventValidator,
    PutEventValidator,
  },
  betValidator: {
    PostBetValidator,
  },
};
