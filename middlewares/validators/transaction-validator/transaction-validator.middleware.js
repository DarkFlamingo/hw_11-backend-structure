const joi = require('joi');
const { HttpError } = require('../../../exceptions/exceptions');

const DefaultValidationResult = (validationSchema, data, next) => {
  const isValidResult = validationSchema.validate(data);
  if (isValidResult.error) {
    throw new HttpError({
      status: 400,
      message: isValidResult.error.details[0].message,
    });
  } else {
    next();
  }
};

const PostTransactionValidator = (req, res, next) => {
  const validationSchema = joi
    .object({
      id: joi.string().uuid(),
      userId: joi.string().uuid().required(),
      cardNumber: joi.string().required(),
      amount: joi.number().min(0).required(),
    })
    .required();
  DefaultValidationResult(validationSchema, req.body, next);
};

module.exports = {
  PostTransactionValidator,
};
