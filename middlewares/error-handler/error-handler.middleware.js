const errorHandler = (err, _req, res, next) => {
  if (res.headersSent) {
    next(err);
  } else {
    const { status = 500, message = '' } = err;
    res.status(status).send({ error: message });
  }
};

module.exports = errorHandler;
