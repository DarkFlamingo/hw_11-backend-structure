const EventApiPath = {
  ROOT: '/',
  $ID: '/:id',
};

module.exports = { EventApiPath };
