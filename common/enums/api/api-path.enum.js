const ApiPath = {
  USERS: '/users',
  TRANSACTIONS: '/transactions',
  STATS: '/stats',
  EVENTS: '/events',
  BETS: '/bets',
  HEALTH: '/health'
}

module.exports = { ApiPath }